from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('newschedule', views.newschedule, name = 'newschedule'),
    path('schedulelist', views.schedulelist, name = 'schedulelist'),
    re_path('delete/(?P<delete_id>.[0-9])', views.delete, name ='delete')
]
