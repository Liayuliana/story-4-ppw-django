from django.shortcuts import render, redirect
from .forms import ScheduleForms
from .models import Schedule
from django.core.exceptions import ValidationError

# Create your views here.

def schedulelist(request):
    myschedule =  Schedule.objects.all()
    context = {
        'page_title' : "My Schedule List",
        'myschedule' : myschedule
    }
    return render(request, 'schedule/listSchedule.html', context)


def newschedule(request):
    context = {
        'page_title' : "Create A New Schedule",
        'schedule_forms': ScheduleForms
    }
    if request.method == 'GET':
        schedule_forms = ScheduleForms()
        context['schedule_forms'] = schedule_forms
        return render(request, 'schedule/newSchedule.html', context)
    else:
        received_form = ScheduleForms(request.POST)
        if received_form.is_valid():
            received_form.save()
            return redirect(schedulelist)
        else:
            context['schedule_forms'] = received_form
            return render(request, 'schedule/newSchedule.html', context)

def delete(request, delete_id):
    Schedule.objects.filter(id=delete_id).delete()
    return redirect(schedulelist)

