from django.db import models

class Schedule(models.Model):
    Date = models.DateField()
    Time = models.TimeField()
    Activity_Name = models.CharField(max_length = 500)
    Activity_Description = models.TextField()
    Place = models.CharField(max_length = 500)

    CATEGORY = [
        ['Academic', 'Academic'],
        ['Family', 'Family'],
        ['Organization', 'Organization'],
        ['Committee', 'Committee'],
        ['UKM', 'UKM'],
        ['Others', 'Others']
    ]
    Category = models.CharField(
        max_length = 500,
        choices = CATEGORY)

    def __str__(self):
        return ("{}.{}".format(self.id, self.Activity_Name))