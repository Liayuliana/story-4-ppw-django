from django import forms
from .models import Schedule
from django.core.exceptions import ValidationError

class ScheduleForms(forms.ModelForm):
    def validate_Activity_Place(value):
        act_name = value
        if act_name.isdigit():
            raise forms.ValidationError("This filed cannot be filled with all numbers")
        else:
            return value
    
    Activity_Name = forms.CharField(
        validators = [validate_Activity_Place],
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id': 'activity_name', 'placeholder' : "Enter activity",
            }
        )
    )

    Activity_Description = forms.CharField(
        validators = [validate_Activity_Place],
        widget = forms.Textarea(
            attrs = {
                'class':'form-control', 'id': 'activity_description', 'placeholder' : "Enter description",
            }
        )
    )

    Place = forms.CharField(
        validators = [validate_Activity_Place],
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'place', 'placeholder' : "Enter place"
            }
        )
    )

    class Meta:
        model = Schedule
        fields = [
            'Date',
            'Time',
            'Activity_Name',
            'Activity_Description',
            'Place',
            'Category'
        ]

        widgets = {
            'Date' : forms.DateInput( 
                attrs = {
                    'type' : 'date', 'id':'date', 'class':'form-control'
                }
            ),

            'Time' : forms.TimeInput(
                attrs = {
                    'type' : 'time', 'id':'time', 'class':'form-control'
                }
            ),

            'Category' : forms.Select(
                attrs = {
                    'type' : 'datalist', 'id':'category', 'class':'form-control',   
                }
            ),
        }
