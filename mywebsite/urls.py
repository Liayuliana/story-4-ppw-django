"""mywebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import url, include
from . import views
from quotes import views as quotesviews
from jadwal import views as jadwalviews


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('quotes', quotesviews.quotes),
    path('newschedule', jadwalviews.newschedule, name="newschedule"),
    path('schedulelist', jadwalviews.schedulelist, name="schedulelist"),
    re_path('delete/(?P<delete_id>.[0-9])', jadwalviews.delete, name="delete")

]